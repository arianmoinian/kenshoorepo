package imageProj.rest.controllers;

import imageProj.exceptions.ApiException;
import imageProj.rest.models.ResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import imageProj.rest.models.ImageDto;
import imageProj.rest.services.ImageService;

import java.util.List;

@RestController
public class ImageController {

    @Autowired
    private ImageService imageService;

    @RequestMapping(value = "/add_image", method = RequestMethod.POST)
    public ResponseDto add(@RequestBody ImageDto imageDto) {
        ResponseDto response = new ResponseDto();
        try {
            imageService.add(imageDto);
            response.setHttpStatusCode(HttpStatus.OK.value());
            response.setStatusStr(HttpStatus.OK.toString());
        }
        catch (ApiException e) {
            response.setHttpStatusCode(e.getHttpStatus().value());
            response.setStatusStr(e.getHttpStatus().toString());
            response.setMsg(e.getMessage());
        }
        catch (Exception e) {
            response.setHttpStatusCode(HttpStatus.BAD_REQUEST.value());
            response.setStatusStr(HttpStatus.BAD_REQUEST.toString());
            response.setMsg(e.getMessage());
        }
        finally {
            return response;
        }
    }

    @RequestMapping(value = "/labels", method = RequestMethod.GET)
    public ResponseDto getLabels(@RequestParam int imageId) {
        ResponseDto response = new ResponseDto();
        try {
            List<String> labels = imageService.getLabelsByImageId(imageId);
            response.setHttpStatusCode(HttpStatus.OK.value());
            response.setPayload(labels);
        }
        catch (ApiException e) {
           response.setHttpStatusCode(e.getHttpStatus().value());
           response.setStatusStr(e.getHttpStatus().toString());
           response.setMsg(e.getMessage());
        }
        catch (Exception e) {
            response.setHttpStatusCode(HttpStatus.BAD_REQUEST.value());
            response.setStatusStr(HttpStatus.BAD_REQUEST.toString());
            response.setMsg(e.getMessage());
        }
        finally {
            return response;
        }
    }
}

package imageProj.rest.models;


import imageProj.dao.model.ImageEntity;

public class ImageDto implements IDto<ImageEntity> {

    private int id;
    private String url;

    public ImageDto(Integer id, String url) {
        this.id = id;
        this.url = url;
    }
    public ImageEntity toEntity() {
        ImageEntity imageEntity = new ImageEntity(this.id, this.url);
        return imageEntity;
    }

    public Integer getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}

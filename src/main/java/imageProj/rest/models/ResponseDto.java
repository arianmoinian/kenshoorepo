package imageProj.rest.models;

public class ResponseDto<PayloadType> {
    private int httpStatusCode;
    private String statusStr;
    private String msg;
    private PayloadType payload;


    public void setHttpStatusCode(int httpStatusCode) {
        this.httpStatusCode = httpStatusCode;
    }


    public void setStatusStr(String statusStr) {
        this.statusStr = statusStr;
    }


    public void setMsg(String msg) {
        this.msg = msg;
    }


    public void setPayload(PayloadType payload) {
        this.payload = payload;
    }

    public int getHttpStatusCode() {
        return httpStatusCode;
    }

    public String getStatusStr() {
        return statusStr;
    }

    public String getMsg() {
        return msg;
    }

    public PayloadType getPayload() {
        return payload;
    }
}

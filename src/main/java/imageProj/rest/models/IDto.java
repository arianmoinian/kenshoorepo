package imageProj.rest.models;

public interface IDto<T> {
    T toEntity();
}

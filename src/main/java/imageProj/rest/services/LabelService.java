package imageProj.rest.services;

import imageProj.dao.model.ImageLabelEntity;
import imageProj.dao.model.LabelEntity;
import imageProj.dao.repositories.ImageLabelRepository;
import imageProj.dao.repositories.LabelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class LabelService {

    @Autowired
    private LabelRepository labelRepository;

    @Autowired
    private ImageLabelRepository imageLabelRepository;

    /**
     * save labels in labels table if not exists already
     * @param labels
     * @return
     */
    @Transactional
    public List<Integer> saveLabels(List<String> labels) {
        List<Integer> labelIds = new ArrayList<>();
        for (String label : labels) {
            LabelEntity labelEntity = new LabelEntity();
            labelEntity.setName(label.toLowerCase());
            LabelEntity labelFromDb;
            Optional<LabelEntity> labelFromDbOpt = labelRepository.findByName(label);
            if (labelFromDbOpt.isPresent()) {
                labelFromDb = labelFromDbOpt.get();
            }
            else {
                labelFromDb = labelRepository.save(labelEntity);
            }
            labelIds.add(labelFromDb.getId());
        }
        return labelIds;
    }

    /**
     * save relation between label and image in image_label table
     * @param imageId
     * @param labels
     */
    @Transactional
    public void saveImageWithLabels(int imageId, List<Integer> labels) {
        for (int labelId : labels) {
            imageLabelRepository.save(new ImageLabelEntity(imageId, labelId));
        }
    }

}

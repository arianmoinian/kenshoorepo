package imageProj.rest.services;

import imageProj.dao.model.ImageEntity;
import imageProj.dao.repositories.ImageRepository;
import imageProj.exceptions.ApiException;
import imageProj.exceptions.ErrorCode;
import imageProj.googleVisionLibrary.GoogleVisionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import imageProj.rest.models.ImageDto;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ImageService {

    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private LabelService labelService;

    @Autowired
    private GoogleVisionService googleVisionService;

    /**
     * add image to db if not exists already. Add connections to labels detected by google vision service
     * @param imageDto
     * @throws IOException
     * @throws ApiException if image already exists or failure in detecting the image meta data
     */
    @Transactional
    public void add(ImageDto imageDto) throws IOException, ApiException {
        validateData(imageDto);
        ImageEntity imageEntity = imageDto.toEntity();
        imageRepository.save(imageEntity);
        List<String> labelsByUrl;
        try {
            labelsByUrl = googleVisionService.getLabelsByUrl(imageDto.getUrl());
        }
        catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // do rollback
            throw e;
        }
        List<Integer> labelIds = labelService.saveLabels(labelsByUrl);
        labelService.saveImageWithLabels(imageDto.getId(), labelIds);
    }

    /**
     * get label descriptions by image id
     * @param imageId
     * @return
     * @throws ApiException if image id does not exist
     */
    public List<String> getLabelsByImageId(int imageId) throws ApiException {
        Optional<ImageEntity> optionalImageEntity = imageRepository.findById(imageId);
        ImageEntity imageEntity = optionalImageEntity.
                orElseThrow(() -> new ApiException(String.format("Image ID %d does not exist", imageId),
                        ErrorCode.IMAGE_ID_DOES_NOT_EXISTS));
        return imageEntity.getLabels().stream().map(x -> x.getName()).collect(Collectors.toList());
    }

    /**
     * checks url is in valid form and that url and image id don't exist already
     * @param imageDto
     * @throws ApiException
     */
    private void validateData(ImageDto imageDto) throws ApiException {
        String url = imageDto.getUrl();
        if (url == null || url.equals("") || !isUrlValid(url)) {
            throw new ApiException(String.format("Not valid Url %s", url),
                    ErrorCode.INPUT_IS_NOT_VALID);
        }
        doesImageExist(imageDto);
    }

    /**
     * checks if url is in a valid form, if not returns false
     * @param url
     * @return
     */
    private boolean isUrlValid(String url) {
        try {
            URL u = new URL(url);
            u.toURI();
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

    /**
     * checks if url or id of image already exists and throws an ApiException if does
     * @param imageDto
     * @throws ApiException
     */
    private void doesImageExist(ImageDto imageDto) throws ApiException {
        List<ImageEntity> byIdOrUrl = imageRepository.getByIdOrUrl(imageDto.getId(), imageDto.getUrl());// check if already exists
        if (byIdOrUrl.size() > 0) {
            if (byIdOrUrl.size() >= 2)
                throw new ApiException(String.format("Both image ID %d and url %s already exist",
                        imageDto.getId(), imageDto.getUrl()), ErrorCode.BOTH_IMAGE_ID_AND_URL_ALREADY_EXIST);
            else { // only one element
                if (byIdOrUrl.get(0).getId() == imageDto.getId())
                    throw new ApiException(String.format("Image ID %d already exists",
                            imageDto.getId()), ErrorCode.IMAGE_ID_ALREADY_EXISTS);
                else
                    throw new ApiException(String.format("Image url %s already exists",
                            imageDto.getUrl()), ErrorCode.IMAGE_URL_ALREADY_EXISTS);
            }
        }
    }

}

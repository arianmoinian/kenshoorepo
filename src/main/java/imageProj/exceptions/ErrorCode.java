package imageProj.exceptions;

import org.springframework.http.HttpStatus;

public enum ErrorCode {
    IMAGE_ID_DOES_NOT_EXISTS(HttpStatus.BAD_REQUEST),
    CANNOT_DETECT_IMAGE_METADATA(HttpStatus.BAD_REQUEST),
    IMAGE_ID_ALREADY_EXISTS(HttpStatus.BAD_REQUEST),
    IMAGE_URL_ALREADY_EXISTS(HttpStatus.BAD_REQUEST),
    BOTH_IMAGE_ID_AND_URL_ALREADY_EXIST(HttpStatus.BAD_REQUEST),
    INPUT_IS_NOT_VALID(HttpStatus.BAD_REQUEST);

    private HttpStatus httpStatus;
    private ErrorCode(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    public HttpStatus getHttpStatus() {
        return this.httpStatus;
    }
}

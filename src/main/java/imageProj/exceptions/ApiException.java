package imageProj.exceptions;

import org.springframework.http.HttpStatus;

public class ApiException extends Exception {
    private ErrorCode errorCode;

    public ApiException(String msg, Throwable cause, ErrorCode errorCode) {
        super(msg, cause);
        this.errorCode = errorCode;
    }

    public ApiException(ErrorCode errorCode) {
        this(null, null, errorCode);
    }

    public ApiException(Throwable cause, ErrorCode errorCode) {
        this(null, cause, errorCode);
    }

    public ApiException(String msg, ErrorCode errorCode) {
        this(msg, null, errorCode);
    }

    public HttpStatus getHttpStatus() {
        return errorCode.getHttpStatus();
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }


}

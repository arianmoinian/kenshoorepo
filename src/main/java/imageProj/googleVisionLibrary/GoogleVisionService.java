package imageProj.googleVisionLibrary;

import com.google.cloud.vision.v1.*;
import imageProj.exceptions.ApiException;
import imageProj.exceptions.ErrorCode;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@Service
public class GoogleVisionService {


    public List<String> getLabelsByUrl(String url) throws IOException, ApiException {
        try (ImageAnnotatorClient client = auth()) {
            List<AnnotateImageRequest> request = buildRequest(url);
            List<AnnotateImageResponse> response = sendRequest(client, request);
            return getLabels(response);
        }
    }


    /**
     * Initialize client that will be used to send requests.
     * Uses the enviroment varibale GOOGLE_APPLICATION_CREDENTIALS to authenticate by oauth 2.0
     */
    private ImageAnnotatorClient auth() throws IOException {
        return ImageAnnotatorClient.create();
    }

    /**
     * @param url
     * @return Builds the request
     */
    private List<AnnotateImageRequest> buildRequest(String url) {
        List<AnnotateImageRequest> requests = new ArrayList<>();
        ImageSource imageSource = ImageSource.newBuilder().setImageUri(url).build();
        Image img = Image.newBuilder().setSource(imageSource).build();
        Feature feat = Feature.newBuilder().setType(Feature.Type.LABEL_DETECTION).build();
        AnnotateImageRequest request = AnnotateImageRequest.newBuilder().addFeatures(feat).setImage(img).build();
        requests.add(request);
        return requests;
    }

    /**
     * @param client the authentication client
     * @param request
     * @return response list
     */
    private List<AnnotateImageResponse> sendRequest(ImageAnnotatorClient client, List<AnnotateImageRequest> request) {
        return client.batchAnnotateImages(request).getResponsesList();
    }

    /**
     * @param response
     * @return the label description
     * @throws ApiException if caanot detect image meta data
     */
    private List<String> getLabels(List<AnnotateImageResponse> response) throws ApiException {
        List<String> labels = new ArrayList<>();
        for (AnnotateImageResponse res : response) {
            if (res.hasError()) {
                throw new ApiException(res.getError().getMessage(),
                        ErrorCode.CANNOT_DETECT_IMAGE_METADATA);
            }

            for (EntityAnnotation annotation : res.getLabelAnnotationsList()) {
                labels.add(annotation.getDescription());
            }
        }
        return labels;
    }



}

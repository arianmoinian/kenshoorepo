package imageProj.dao.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "images")
public class ImageEntity {

        @Id
        @Column(name = "Id", nullable = false)
        private int id;

        @Column(name = "Url", nullable = false, unique = true)
        private String url;


        @ManyToMany(fetch = FetchType.LAZY)
        @JoinTable(name = "image_label",
            joinColumns = @JoinColumn(name = "Image_Id", nullable = false),
                inverseJoinColumns = @JoinColumn(name = "Label_Id", nullable = false))
        private Set<LabelEntity> labels;


        public ImageEntity() {}

        public ImageEntity(int id, String url) {
            this.id = id;
            this.url = url;
        }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Set<LabelEntity> getLabels() {
        return labels;
    }

    public void setLabels(Set<LabelEntity> labels) {
        this.labels = labels;
    }
}

package imageProj.dao.model;


import javax.persistence.*;

@Entity
@Table(name = "image_label")
public class ImageLabelEntity {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "Id", nullable = false)
        private int id;

        @Column(name = "Image_Id", nullable = false)
        private int imageId;

        @Column(name = "Label_Id", nullable = false)
        private int labelId;

        public ImageLabelEntity() {}
        public ImageLabelEntity(int imageId, int labelId) {
                this.imageId = imageId;
                this.labelId = labelId;
        }

}

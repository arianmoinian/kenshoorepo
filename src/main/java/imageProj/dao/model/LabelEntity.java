package imageProj.dao.model;

import javax.persistence.*;

@Entity
@Table(name = "labels")
public class LabelEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id", nullable = false)
    private int id;

    @Column(name = "Name", nullable = false, unique = true)
    private String name;

    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof LabelEntity))
            return false;
        return this.id == ((LabelEntity) o).id;
    }

    @Override
    public int hashCode() {
        return Long.hashCode(this.id);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

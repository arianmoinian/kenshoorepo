package imageProj.dao.repositories;

import imageProj.dao.model.ImageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ImageRepository extends JpaRepository<ImageEntity, Integer> {

    @Query("select i from ImageEntity i where i.id = :id or i.url = :url")
    List<ImageEntity> getByIdOrUrl(@Param("id") int id, @Param("url") String url);
}

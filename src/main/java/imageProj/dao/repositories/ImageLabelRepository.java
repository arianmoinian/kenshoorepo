package imageProj.dao.repositories;

import imageProj.dao.model.ImageLabelEntity;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ImageLabelRepository extends JpaRepository<ImageLabelEntity, Integer> {


}

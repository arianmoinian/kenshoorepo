package imageProj.dao.repositories;

import imageProj.dao.model.LabelEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface LabelRepository extends JpaRepository<LabelEntity, Integer> {
    @Query("select l from LabelEntity l where l.name = :name")
    Optional<LabelEntity> findByName(@Param("name") String name);
}

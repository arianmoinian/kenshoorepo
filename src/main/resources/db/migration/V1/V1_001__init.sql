CREATE TABLE `imageprojdb`.`images` (
  `Id` INT NOT NULL,
  `Url` VARCHAR(500) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE INDEX `Url_UNIQUE` (`Url` ASC) VISIBLE
  );

CREATE TABLE `imageprojdb`.`labels` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE
  );

CREATE TABLE `imageprojdb`.`image_label` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `Image_Id` INT NULL,
  `Label_Id` INT NULL,
  PRIMARY KEY (`Id`),
  INDEX `Image_Id_idx` (`Image_Id` ASC) VISIBLE,
  INDEX `Label_Id_idx` (`Label_Id` ASC) VISIBLE,
  CONSTRAINT `Image_Id`
    FOREIGN KEY (`Image_Id`)
    REFERENCES `imageprojdb`.`images` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `Label_Id`
    FOREIGN KEY (`Label_Id`)
    REFERENCES `imageprojdb`.`labels` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
    );

